
$(function() {




// Лайк продукта
$('.product-item__like').click(function() {
  $(this).toggleClass('active');
})


// FansyBox
 $('.fancybox').fancybox({});





$('.banner-slider').slick({
  dots: true,
  arrows: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true
  // autoplay: true,
  // autoplaySpeed: 5000,
});





$('.product-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 479,
      settings: {
        slidesToShow: 1,
      }
    },
    ]
});






var productSlider = $('.product-card__slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  dots: false,
  asNavFor: '.product-card__slider--nav'
});


$('.product-card__slider--nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.product-card__slider',
  dots: false,
  arrows: true,
  focusOnSelect: true,
  responsive: [
  {
    breakpoint: 768,
    settings: {
      slidesToShow: 2,
    }
  },
  {
    breakpoint: 479,
    settings: {
      slidesToShow: 1,
    }
  },
  ]
});




 // Стилизация селектов
$('select').styler();






$(".phone-mask").mask("+7 (999) 999-99-99");



// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 100,
  max: 10000,
  values: [ 2000, 5600 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range





// сайдбар на мобильном
$('.sidebar-filter__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})

})